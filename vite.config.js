// vite.config.js
import { defineConfig } from "vite";
import { svelte } from "@sveltejs/vite-plugin-svelte";
import cssInjectedByJsPlugin from "vite-plugin-css-injected-by-js";

// https://vitejs.dev/config/
export default defineConfig({
  base: "", // Do not prepend output paths in index.html with slash
  plugins: [
    svelte(),
    cssInjectedByJsPlugin({
      cssAssetsFilterFunction: function customCssAssetsFilterFunction(
        outputAsset
      ) {
        return outputAsset.fileName == "inlaympsupport.css";
      },
      injectCodeFunction: function injectCodeCustomRunTimeFunction(
        cssCode,
        options
      ) {
        try {
          if (typeof document != "undefined") {
            var elementStyle = document.createElement("style");
            elementStyle.appendChild(document.createTextNode(cssCode));
            document.head.appendChild(elementStyle);
          }
        } catch (e) {
          console.error("vite-plugin-css-injected-by-js", e);
        }
      }
    })
  ],
  build: {
    outDir: "./dist",
    emptyOutDir: false,
    rollupOptions: {
      input: {
        // the key is the output filename without extension.
        // the value is where to find the input.
        inlaympsupport: "index.html"
        // 'other': "src/other.scss",
      },
      output: {
        // The following ensure we don't have random hashes in output.
        entryFileNames: `[name].js`,
        // chunkFileNames: `[name].js`,
        assetFileNames: `[name].[ext]`
      }
    }
  }
});
