import InlayPoliticalSupport from "./InlayPoliticalSupport.svelte";
import PartySummary from "./PartySummary.svelte";

if (!("inlayMPSupportInit" in window)) {
  // This is the first time this *type* of Inlay has been encountered.
  // We need to define anything global here.

  // Create the boot function.
  window.inlayMPSupportInit = inlay => {
    const containerNode = document.createElement("div");
    inlay.script.insertAdjacentElement("afterend", containerNode);
    if ((inlay.script.dataset.view ?? "PartySummary") === "PartySummary") {
      new PartySummary({
        target: containerNode,
        props: {
          inlay
        }
      });
    } else {
      new InlayPoliticalSupport({
        target: containerNode,
        props: {
          inlay
        }
      });
    }
  };
}
