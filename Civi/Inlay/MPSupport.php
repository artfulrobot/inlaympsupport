<?php

namespace Civi\Inlay;

use CRM_Inlaympsupport_ExtensionUtil as E;
use Civi\Api4\Activity;
use Civi\Api4\Contact;
use Civi\Api4\OptionValue;

class MPSupport extends Type {

  /**
   * Typically the version from the info.xml file, but only needs updating when your config changes.
   *
   * @const string
   */
  const CONFIG_VERSION = '2.0';

  public static $typeName = 'MP support';

  public static $defaultConfig = [
    'issueOptionValue' => [],
    'publicTitle' => '',
    'introHTML' => '',
    'ctaHTML' => '',
    'partyPlaceholders' => [],
    'uxMode' => 'allActivitiesSitting', /* allActivities|allActivitiesSitting|allSitting|allActivitiesStanding|allStanding */
  ];

  /**
   * Note: because of the way CRM.url works, you MUST put a ? before the #
   *
   * @var string
   */
  public static $editURLTemplate = 'civicrm/a?#/inlays/mpsupport/{id}';

  /**
   * Generates data to be served with the Javascript application code bundle.
   *
   * Structure:
   * {
   *   init: 'inlayMPSupportInit',
   *   publicTitle,
   *   introHTML,
   *   ctaHTML,
   *   partyPlaceholders, // ??
   *   uxMode,
   *   parties: {
   *     <partyName>: { label }
   *   },
   *   people: [
   *     {
   *       name,
   *       cnst,
   *       prty: <partyName>,
   *       ?i<issueNo>: {
   *           q: 'Supports|Unknown|Unconvinced|Opposed',
   *           h: true/false,
   *           s: ?'I support this because...'
   *       }
   *     }
   *   ]
   * }
   *
   * @return array
   */
  public function getInitData(): array {
    $data = [
      // Name of global Javascript function used to boot this app.
      'init'             => 'inlayMPSupportInit',
      // 'constituencyMap' => \Civi\ConstituencyChanges2024::$map,
    ];
    foreach (['publicTitle', 'introHTML', 'ctaHTML', 'partyPlaceholders', 'uxMode'] as $_) {
      $data[$_] = $this->config[$_] ?? '';
    }

    // Fetch all the known support activities for these issues.
    $apiCall = Activity::get(FALSE)
      ->addSelect(
        'target_contact_id',
        'Shown_Support_for.Issue',
        'Shown_Support_for.Highlight_at_top_of_list',
        'Shown_Support_for.Quality_of_support_for_issue:name',
        'Shown_Support_for.Public_statement'
      )
      ->addJoin('Contact AS contact', 'INNER', 'ActivityContact', ['contact.contact_sub_type', 'CONTAINS', '"MP"'])
      ->addWhere('activity_type_id:name', '=', 'Shown support')
      ->addWhere('Shown_Support_for.Issue', 'IN', $this->config['issueOptionValue']);
    $restrictBySitting = preg_match('/^(allActivitiesSitting|allSitting)$/', $this->config['uxMode']);
    $restrictByStanding = preg_match('/^(allActivitiesStanding|allStanding)$/', $this->config['uxMode']);
    if ($restrictBySitting) {
      $apiCall->addWhere('contact.MPs_Info.Is_sitting', '=', 1);
    }
    if ($restrictByStanding) {
      $apiCall->addWhere('contact.MPs_Info.Is_standing', '=', 1);
    }
    // Restructure into $contacts[$ctID]['q'][$issue] = 'Supports|Opposed...'
    $knownCts = [];
    foreach ($apiCall->execute() as $activity) {
      $issue = $activity['Shown_Support_for.Issue'];
      foreach ($activity['target_contact_id'] as $ct) {
        $knownCts[$ct][$issue] = [
          'q' => $activity['Shown_Support_for.Quality_of_support_for_issue:name'],
          'h' => $activity['Shown_Support_for.Highlight_at_top_of_list'],
          's' => $activity['Shown_Support_for.Public_statement'],
        ];
      }
    }

    // Fetch all the contacts we need, in name order.
    $apiCall = Contact::get(FALSE)
      ->addSelect(
        'display_name',
        'MPs_Info.Party', /* option group value */
        'MPs_Info.Constituency',
      )
      ->addWhere('contact_sub_type', 'CONTAINS', 'MP')
      ->addOrderBy('first_name')
      ->addOrderBy('last_name');
    if (preg_match('/^allActivities/', $this->config['uxMode'])) {
      // Restrict to those with activities.
      $apiCall->addWhere('id', 'IN', array_keys($knownCts));
    }
    else {
      if ($restrictByStanding) {
        $apiCall->addWhere('MPs_Info.Is_standing', '=', 1);
      }
      if ($restrictBySitting) {
        $apiCall->addWhere('MPs_Info.Is_sitting', '=', 1);
      }
    }
    $ctDetail = $apiCall->execute();

    // We need party details too.
    $data['parties'] = [];
    $optionValues = \Civi\Api4\OptionValue::get(FALSE)
      ->addSelect('value', 'label')
      ->addWhere('option_group_id:name', '=', 'party_20200128194847')
      ->execute();
    foreach ($optionValues as $optionValue) {
      // do something
      unset($optionValue['id']);
      $data['parties'][$optionValue['value']] = $optionValue;
    }

    $data['issues'] = OptionValue::get(FALSE)
      ->addSelect('value', 'label')
      ->addWhere("option_group_id:name", "=", "issue_20201121080319")
      ->addWhere('value', 'IN', $this->config['issueOptionValue'])
      ->execute()
      ->indexBy('value')->column('label');
    $data['people'] = [];

    foreach ($ctDetail as $ct) {
      \Civi\ConstituencyChanges2024::preferNew($ct['MPs_Info.Constituency']);
      $record = [
        'name' => $ct['display_name'],
        'cnst' => $ct['MPs_Info.Constituency'],
        'prty' => $ct['MPs_Info.Party'],
      ];
      foreach ($knownCts[$ct['id']] ?? [] as $issue => $support) {
        // We use an 'i' prefix because numeric keys can get weird between PHP and JS
        $record["i$issue"] = $support;
      }
      $data['people'][] = $record;
    }

    // This sort sorts by support.
    //
    // foreach  {
    //   usort($party['mps'], function ($a, $b) {
    //     $cmp = $b['h'] <=> $a['h'];
    //     $map = ['Supports' => 1, 'Unconvinced' => 2, 'Opposed' => 3];
    //     if (!$cmp) {
    //       $cmp = $map[$a['q']] <=> $map[$b['q']];
    //     }
    //     if (!$cmp) {
    //       $cmp = $a['n'] <=> $b['n'];
    //     }
    //     $a = (($cmp < 0) ? "putting $a[n] before $b[n]"
    //     : (
    //       ($cmp > 0) ? "putting $a[n] after $b[n]" : "same"
    //     )
    //   );
    //     return $cmp;
    //   });
    // }

    return $data;
  }

  /**
   * Process a request (not required)
   *
   * Request data is just key, value pairs from the form data. If it does not
   * have 'token' field then a token is generated and returned. Otherwise the
   * token is checked and processing continues.
   *
   * @param \Civi\Inlay\Request $request
   * @return array
   *
   * @throws \Civi\Inlay\ApiException;
   */
  public function processRequest(ApiRequest $request): array {

    return ['success' => 1];
  }

  /**
   * Returns a URL to a page that lets an admin user configure this Inlay.
   *
   * @return string URL
   */
  public function getAdminURL() {
  }

  /**
   * Get the Javascript app script.
   *
   * This will be bundled with getInitData() and some other helpers into a file
   * that will be sourced by the client website.
   *
   * @return string Content of a Javascript file.
   */
  public function getExternalScript(): string {
    return file_get_contents(E::path('dist/inlaympsupport.js'));
  }

  /**
   * Config migrations.
   */
  protected function migrateConfig(array $config): array {
    // ... your migrations here ...

    if (empty($config['version'])) {
      // Change 'issueOptionValue' from a ?int to [int]
      if ($config['issueOptionValue'] && !is_array($config['issueOptionValue'])) {
        $config['issueOptionValue'] = [$config['issueOptionValue']];
      }
    }
    $config['version'] = static::CONFIG_VERSION;

    return $config;
  }

}
