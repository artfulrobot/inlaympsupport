# inlaympsupport

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Requirements

- PHP v7.4+
- CiviCRM 5.65

## Use

Add `data-view="PartySummary"` to the `<script>` tag.

